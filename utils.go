package main

import (
	"context"
	"os/exec"
	"strings"
	"os"
	"fmt"

	rspec "github.com/opencontainers/runtime-spec/specs-go"
	imgspecv1 "github.com/opencontainers/image-spec/specs-go/v1"
	"github.com/containers/image/v5/copy"
	"github.com/containers/image/v5/docker"
	"github.com/containers/image/v5/signature"
	"github.com/containers/image/v5/transports/alltransports"
	"github.com/containers/image/v5/pkg/blobinfocache"
	"github.com/containers/image/v5/types"
	"github.com/containers/storage/pkg/unshare"
	"github.com/opencontainers/runtime-tools/generate"

	// TODO: inspect how big this dependency actually is
	"github.com/docker/docker/pkg/archive"
)

func GetImageReference(image string) types.ImageReference {
	// TODO: Add support for all the other registry types
	if strings.HasPrefix(image, "docker://") {
		docker_image := strings.TrimPrefix(image, "docker:")

		ref, err := docker.ParseReference(docker_image)
		if err != nil {
			panic(err)
		}

		return ref
	}

	panic("Unsupported image transport. Prefix your image name with docker://")
}

func RunCMD(cmd *exec.Cmd) {
	fmt.Println("Running the following command:", cmd.Args)
	cmd.Stdout = os.Stdout
    cmd.Stderr = os.Stderr
    err := cmd.Run()
	if err != nil {
		panic(err)
	}
}

func PullAndUnpack(srcRef types.ImageReference, rootfs_path string) {
	// Create the various contexts(?) for the various operations we will perform
	sourceCtx := newSystemContext()
	destinationCtx := newSystemContext()
	ctx := context.Background()

	// Pull the source image, and store it in the local storage, under the name main
	policy, err := signature.DefaultPolicy(nil)
	policyContext, err := signature.NewPolicyContext(policy)
	destRef, err := alltransports.ParseImageName("containers-storage:main")
	if err != nil {
		panic(err)
	}
	_, err = copy.Image(ctx, policyContext, destRef, srcRef,
	                    &copy.Options{
		RemoveSignatures:      true,
		SignBy:                "",
		ReportWriter:          os.Stdout,
		SourceCtx:             sourceCtx,
		DestinationCtx:        destinationCtx,
		ForceManifestMIMEType: "",
		ImageListSelection:    copy.CopySystemImage,
		OciDecryptConfig:      nil,
		OciEncryptLayers:      nil,
		OciEncryptConfig:      nil,
	})
	if err != nil {
		panic(err)
	}

	// Now that the copy has happened, open the dest ref as a (source) image
	img, err := destRef.NewImage(ctx, nil)
	if err != nil {
		panic(err)
	}
	defer img.Close()
	imgSrc, err := destRef.NewImageSource(ctx, nil)
	if err != nil {
		panic(err)
	}
	defer imgSrc.Close()

	// Get the list of layers that compose the image, and unpack them
	blobInfoCache := blobinfocache.DefaultCache(destinationCtx)
	layers, err := img.LayerInfosForCopy(ctx)
	if err != nil {
		panic(err)
	}
	for _, layerBlob := range layers {
		srcStream, _, err := imgSrc.GetBlob(ctx, layerBlob, blobInfoCache)
		if err != nil {
			panic(err)
		}
		defer srcStream.Close()

		if err := archive.Untar(srcStream, rootfs_path, &archive.TarOptions{
			NoLchown: true,
		}); err != nil {
			panic(err)
		}
	}
}

func ImageConfigtoRunTimeSpec(imgconfig imgspecv1.ImageConfig, rootfs string) generate.Generator {
	generator, err := generate.New("linux")
	if err != nil {
		panic(err)
	}

	generator.SetVersion(rspec.Version)
	generator.SetupPrivileged(true)

	generator.Config.Hostname = "boot2container"
	generator.Config.Linux.Resources.Devices[0].Allow = true
	generator.Config.Process.Env = imgconfig.Env
	generator.Config.Process.Args = append(imgconfig.Entrypoint, imgconfig.Cmd...)

	if len(imgconfig.WorkingDir) > 0 {
		generator.Config.Process.Cwd = imgconfig.WorkingDir
	} else {
		generator.Config.Process.Cwd = "/"
	}

	// Give full access to the host network
	generator.RemoveLinuxNamespace("network")

	return generator
}

func GenOCIConfig(srcRef types.ImageReference, config_path, rootfs_path string) {
	// Get the image associated to the source image
	ctx := context.Background()
	img, err := srcRef.NewImage(ctx, nil)
	if err != nil {
		panic(err)
	}
	defer img.Close()

	ociconfig, err := img.OCIConfig(ctx)
	if err != nil {
		panic(err)
	}

	// Generate the container configuration
	runtime_spec := ImageConfigtoRunTimeSpec(ociconfig.Config, rootfs_path)
	runtime_spec.SaveToFile(config_path, generate.ExportOptions{Seccomp: false})
}

func RunContainer(image, root, cache_dir string) {
	rootfs_path := root+"/rootfs"
	config_path := root+"/config.json"

	unshare.MaybeReexecUsingUserNamespace(false)

	// TODO: Check the root arg is a path to a directory

	// Get the reference images
	srcRef, err := alltransports.ParseImageName(image)
	if err != nil {
		panic(err)
	}

	// Pull the image and generate the rootfs
	// TODO: Stop using img, and instead do that ourselves
	//ImgPullAndUnpack(image, root + "/rootfs", cache_dir)

	PullAndUnpack(srcRef, rootfs_path)

	GenOCIConfig(srcRef, config_path, rootfs_path)

	// Start
	cmd := exec.Command("crun", "run", "--no-pivot", "main")
	cmd.Dir = root
	RunCMD(cmd)
}

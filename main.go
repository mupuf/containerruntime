package main

import (
	"fmt"
	"os"

	"github.com/containers/image/v5/types"
	"github.com/containers/storage/pkg/reexec"
	"github.com/spf13/cobra"
)

var globalArgs struct {
	root *string
	cache *string
	registriesConfPath *string
}

func newSystemContext() *types.SystemContext {
	ctx := &types.SystemContext{
		RegistriesDirPath:        "/etc/containers/registries.d",
		ArchitectureChoice:       "",
		OSChoice:                 "",
		VariantChoice:            "",
		SystemRegistriesConfPath: *globalArgs.registriesConfPath,
		BigFilesTemporaryDir:     "", //*globalArgs.cache + "/tmp",
	}
	return ctx
}

var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Run an image",
	Long:  `Pull an image, cache its layers, create a container, and execute it`,
	Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		RunContainer(args[0], *globalArgs.root, *globalArgs.cache)
	},
}

var rootCmd = &cobra.Command{
	Use:   "crt",
	Short: "CRT is a simple container runtime",
	Long: `A simple container runtime that pulls images, caches layers, and runs containers`,
	Run: func(cmd *cobra.Command, args []string) {
	},
}

func main() {
	// Initial setup
	if reexec.Init() {
		os.Exit(1)
	}

	// Args
	globalArgs.cache = rootCmd.PersistentFlags().String("cache", "/container/cache", "Folder that will be used to cache layers")
	globalArgs.root = rootCmd.PersistentFlags().String("root", "/container/", "Directory which will serve as the containers' root")
	globalArgs.registriesConfPath = rootCmd.PersistentFlags().String("registriesConfPath", "/etc/containers/registries.conf", "Path to the registry configuration file")

	// Define the commands
	rootCmd.AddCommand(runCmd)

	// Execute the wanted command
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(2)
	}
}

module gitlab.freedesktop.org/mupuf/boot2container/crt

go 1.15

require (
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/containers/image/v5 v5.9.0
	github.com/containers/storage v1.23.7
	github.com/docker/docker v1.4.2-0.20191219165747-a9416c67da9f
	github.com/opencontainers/image-spec v1.0.2-0.20190823105129-775207bd45b6
	github.com/opencontainers/runtime-spec v1.0.3-0.20200520003142-237cc4f519e2
	github.com/opencontainers/runtime-tools v0.9.0
	github.com/spf13/cobra v1.1.1
)
